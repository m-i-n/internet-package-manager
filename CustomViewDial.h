//
//  CustomViewDial.h
//  Internet Packages 2
//
//  Created by Nayem on 8/2/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomViewDial : UIView

//@property (nonatomic, strong) UILabel *dial_rechargeNameLabel;

@property (nonatomic, strong) UILabel *volumeLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *validityLabel;
@property (nonatomic, strong) UILabel *utLabel;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *dial_rechargeLabel;

@end
