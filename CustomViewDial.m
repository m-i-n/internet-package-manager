//
//  CustomViewDial.m
//  Internet Packages 2
//
//  Created by Nayem on 8/2/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import "CustomViewDial.h"

#define FIRST_LABEL_HEIGHT_PERCENT 0.10
#define SECOND_LABEL_HEIGHT_PERCENT 0.10
#define THIRD_LABEL_HEIGHT_PERCENT 0.10
#define FOURTH_LABEL_HEIGHT_PERCENT 0.10
#define FIFTH_LABEL_HEIGHT_PERCENT 0.10
#define SIXTH_LABEL_HEIGHT_PERCENT 0.10
#define BUTTON_HEIGHT_PERCENT 0.10

@implementation CustomViewDial

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        // initilize all your UIView components
        CGFloat startX = 16;
        CGFloat startY = 60;
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = (width - startX)/2;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        
        //UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(20,30, 200, 44)];
        //UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(20,80, 200, 44)];
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY, width, height*FIRST_LABEL_HEIGHT_PERCENT)];
        self.volumeLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX+width, startY, width, height*FIRST_LABEL_HEIGHT_PERCENT)];
        
        label1.text = @"Volume";
        self.volumeLabel.text = @"Data will be provided";
        
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY+height*FIRST_LABEL_HEIGHT_PERCENT, width, height*SECOND_LABEL_HEIGHT_PERCENT)];
        self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX+width, startY+height*FIRST_LABEL_HEIGHT_PERCENT, width, height*SECOND_LABEL_HEIGHT_PERCENT)];
        
        label2.text = @"Price";
        self.priceLabel.text = @"provided";
        
        
        UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY+height*(FIRST_LABEL_HEIGHT_PERCENT + SECOND_LABEL_HEIGHT_PERCENT ), width, height*THIRD_LABEL_HEIGHT_PERCENT)];
        self.validityLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX+width, startY+height*(FIRST_LABEL_HEIGHT_PERCENT + SECOND_LABEL_HEIGHT_PERCENT ), width, height*THIRD_LABEL_HEIGHT_PERCENT)];
        
        label3.text = @"Validity";
        self.validityLabel.text = @"provided";
        
        
        UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY+height*(FIRST_LABEL_HEIGHT_PERCENT + SECOND_LABEL_HEIGHT_PERCENT + THIRD_LABEL_HEIGHT_PERCENT ), width, height*FOURTH_LABEL_HEIGHT_PERCENT)];
        self.utLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX+width, startY+height*(FIRST_LABEL_HEIGHT_PERCENT + SECOND_LABEL_HEIGHT_PERCENT + THIRD_LABEL_HEIGHT_PERCENT ), width, height*FOURTH_LABEL_HEIGHT_PERCENT)];
        
        label4.text = @"Usage Time";
        self.utLabel.text = @"provided";
        
        
        UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY+height*(FIRST_LABEL_HEIGHT_PERCENT + SECOND_LABEL_HEIGHT_PERCENT + THIRD_LABEL_HEIGHT_PERCENT + FOURTH_LABEL_HEIGHT_PERCENT ), width, height*FIFTH_LABEL_HEIGHT_PERCENT)];
        self.typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX+width, startY+height*(FIRST_LABEL_HEIGHT_PERCENT + SECOND_LABEL_HEIGHT_PERCENT + THIRD_LABEL_HEIGHT_PERCENT + FOURTH_LABEL_HEIGHT_PERCENT ), width, height*FIFTH_LABEL_HEIGHT_PERCENT)];
        
        label5.text = @"Type";
        self.typeLabel.text = @"provided";
        
        
        // this should be public label as dial_rechargeNameLabel...it'll be updated by the selection of row
        UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY+height*(FIRST_LABEL_HEIGHT_PERCENT + SECOND_LABEL_HEIGHT_PERCENT + THIRD_LABEL_HEIGHT_PERCENT + FOURTH_LABEL_HEIGHT_PERCENT + FIFTH_LABEL_HEIGHT_PERCENT ), width, height*SIXTH_LABEL_HEIGHT_PERCENT)];
        self.dial_rechargeLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX+width, startY+height*(FIRST_LABEL_HEIGHT_PERCENT + SECOND_LABEL_HEIGHT_PERCENT + THIRD_LABEL_HEIGHT_PERCENT + FOURTH_LABEL_HEIGHT_PERCENT + FIFTH_LABEL_HEIGHT_PERCENT ), width, height*SIXTH_LABEL_HEIGHT_PERCENT)];
        
        label6.text = @"Dial";
        self.dial_rechargeLabel.text = @"provided";
        
        
        UIButton *getButton = [[UIButton alloc]initWithFrame:CGRectMake(startX, startY+height*(FIRST_LABEL_HEIGHT_PERCENT + SECOND_LABEL_HEIGHT_PERCENT + THIRD_LABEL_HEIGHT_PERCENT + FOURTH_LABEL_HEIGHT_PERCENT + FIFTH_LABEL_HEIGHT_PERCENT + SIXTH_LABEL_HEIGHT_PERCENT), width, height*BUTTON_HEIGHT_PERCENT)];
        getButton.backgroundColor = [UIColor orangeColor];
        getButton.titleLabel.text = @"Get";
        
        
        [self addSubview:label1];
        [self addSubview:self.volumeLabel];
        [self addSubview:label2];
        [self addSubview:self.priceLabel];
        [self addSubview:label3];
        [self addSubview:self.validityLabel];
        [self addSubview:label4];
        [self addSubview:self.utLabel];
        [self addSubview:label5];
        [self addSubview:self.typeLabel];
        [self addSubview:label6];
        [self addSubview:self.dial_rechargeLabel];
        [self addSubview:getButton];
        
    }
    return self;
}

@end
