//
//  DBManager.h
//  SQLite3Sample1
//
//  Created by Nayem on 7/20/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBManager : NSObject

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

@property (nonatomic, strong) NSMutableArray *arrColumnNames;
@property (nonatomic) int affectedRows;
@property (nonatomic) long long lastInsertedRowID;

-(NSArray *)loadDataFromDB:(NSString *)query;       //To load data from a database

-(void)executeQuery:(NSString *)query;              //For updating a database

@end
