//
//  PackageDetailsViewController.m
//  Internet Packages 2
//
//  Created by Nayem on 7/26/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import "DialPackageDetailsViewController.h"
//#import "CustomViewDial.h"
#import "DBManager.h"
#import "PackageTableViewController.h"

@interface DialPackageDetailsViewController ()
@property (weak, nonatomic) IBOutlet UITextField *volumeTextField;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UITextField *validityTextField;
@property (weak, nonatomic) IBOutlet UITextField *usagetimeTextField;
@property (weak, nonatomic) IBOutlet UITextField *typeTextField;
@property (weak, nonatomic) IBOutlet UITextField *dialTextField;

@end

@implementation DialPackageDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //NSLog(@"PackageDetailsViewController loaded");
    
    NSInteger indexOfVolume = [self.dbManager.arrColumnNames indexOfObject:@"volume"];
    NSInteger indexOfPrice = [self.dbManager.arrColumnNames indexOfObject:@"price"];
    NSInteger indexOfValidity = [self.dbManager.arrColumnNames indexOfObject:@"validity"];
    NSInteger indexOfType = [self.dbManager.arrColumnNames indexOfObject:@"generation"];
    NSInteger indexOfActivationCode = [self.dbManager.arrColumnNames indexOfObject:@"dial_recharge"];
    
    self.volumeTextField.text = [self.currentPackageInfo objectAtIndex:indexOfVolume];
    self.priceTextField.text = [self.currentPackageInfo objectAtIndex:indexOfPrice];
    self.validityTextField.text = [self.currentPackageInfo objectAtIndex:indexOfValidity];
    //self.usagetimeTextField.text = @"No DB Entry";
    self.typeTextField.text = [self.currentPackageInfo objectAtIndex:indexOfType];
    self.dialTextField.text = [self.currentPackageInfo objectAtIndex:indexOfActivationCode];
    //NSLog(@"%@",[self.currentPackageInfo objectAtIndex:indexOfVolume]);
    
    //create an instance of your custom view...For now, this isn't needed anymore......
    /*
    CustomViewDial *cv = [[CustomViewDial alloc]initWithFrame:self.view.frame];
    [self.view addSubview:cv];
     */
}
- (IBAction)makePhoneCall:(id)sender {
    NSLog(@"Call button pressed");
//    NSString *phoneNumber = @"tel://+88validphonenumber";
    //NSString *phoneNumber = [@"tel://" stringByAppendingString:self.dialTextField.text];
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:self.dialTextField.text];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneNumber]]) {
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber] options:@{} completionHandler:^(BOOL success) {
                
                if (success) {
                    NSLog(@"Call to %@ worked",[phoneNumber uppercaseString]);
                } else {
                    NSLog(@"Call to %@ didn't work",[phoneNumber uppercaseString]);
                }
                
            }];
        } else {
            BOOL success = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
            NSLog(@"Call to %@ %@", [phoneNumber uppercaseString], success?@"worked":@"didn't work");
        }
    } else {
        UIAlertController *alertController = [[UIAlertController alloc]init];
        alertController.title = @"Wait";
        alertController.message = @"Not yet implemented";
        
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction: alertAction];
        [self presentViewController:alertController animated:YES completion:^{
            
        }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
