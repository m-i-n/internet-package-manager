//
//  Operators.h
//  Go
//
//  Created by Nayem on 7/16/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Operators : NSObject

@property (nonatomic) NSString *name;
@property (nonatomic) NSURL *filepath;

@end
