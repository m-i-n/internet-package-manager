//
//  PackageTableViewController.h
//  Internet Packages 1
//
//  Created by Nayem on 7/22/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Operators.h"
#import "MyTableViewCell.h"
#import "MyTableViewHeaderCell.h"
#import "DialPackageDetailsViewController.h"
#import "DBManager.h"

@interface PackageTableViewController : UITableViewController

@property (nonatomic, strong) Operators *currentOperator;
@property (strong, nonatomic) IBOutlet UITableView *packageTable;

@end
