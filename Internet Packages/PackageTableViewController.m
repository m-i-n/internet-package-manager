//
//  PackageTableViewController.m
//  Internet Packages 1
//
//  Created by Nayem on 7/22/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import "PackageTableViewController.h"
#import "RechargePackageDetailsViewController.h"


@interface PackageTableViewController ()

@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSArray *arrPrepaid2gPackageInfo;
@property (nonatomic, strong) NSArray *arrPrepaid3gPackageInfo;
@property (nonatomic, strong) NSArray *arrPostpaid2gPackageInfo;
@property (nonatomic, strong) NSArray *arrPostpaid3gPackageInfo;

@end

@implementation PackageTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.currentOperator.name;
    self.packageTable.dataSource = self;
    self.packageTable.delegate = self;
    
    // Initialize the dbManager property.
    self.dbManager = [[DBManager alloc]initWithDatabaseFilename:@"myDB.db"];
    [self.tableView registerClass:[MyTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView registerClass:[MyTableViewHeaderCell class] forCellReuseIdentifier:@"headerCell"];
    // Load the data
    [self loadData];
}

- (void)loadData{
    // Form the query of the format
    // select * from GrameenphonePackageInfo where generation='3G' and paidtype='pre'
    NSString *queryPrepaid2g = [[@"select * from " stringByAppendingString:[self.currentOperator.name stringByAppendingString:@"PackageInfo"]]stringByAppendingString:[NSString stringWithFormat:@" where generation='2G' and paidtype='pre'"]];
    NSString *queryPrepaid3g = [[@"select * from " stringByAppendingString:[self.currentOperator.name stringByAppendingString:@"PackageInfo"]]stringByAppendingString:[NSString stringWithFormat:@" where generation='3G' and paidtype='pre'"]];
    NSString *queryPostpaid2g = [[@"select * from " stringByAppendingString:[self.currentOperator.name stringByAppendingString:@"PackageInfo"]]stringByAppendingString:[NSString stringWithFormat:@" where generation='2G' and paidtype='post'"]];
    NSString *queryPostpaid3g = [[@"select * from " stringByAppendingString:[self.currentOperator.name stringByAppendingString:@"PackageInfo"]]stringByAppendingString:[NSString stringWithFormat:@" where generation='3G' and paidtype='post'"]];

    //NSLog(@"%@",queryPrepaid2g);
    //NSLog(@"%@",queryPrepaid3g);
    //NSLog(@"%@",queryPostpaid2g);
    //NSLog(@"%@",queryPostpaid3g);

    
    // Check if the array contains any previous data. If it contains previous data, free them....
    if (self.arrPrepaid2gPackageInfo!=nil) {
        self.arrPrepaid2gPackageInfo = nil;
    }
    if (self.arrPrepaid3gPackageInfo!=nil) {
        self.arrPrepaid3gPackageInfo = nil;
    }
    if (self.arrPostpaid2gPackageInfo!=nil) {
        self.arrPostpaid2gPackageInfo = nil;
    }
    if (self.arrPostpaid3gPackageInfo!=nil) {
        self.arrPostpaid3gPackageInfo = nil;
    }
    // Fetch the data
    self.arrPrepaid2gPackageInfo = [self.dbManager loadDataFromDB:queryPrepaid2g];
    //NSLog(@"First array: %@",self.arrPrepaid2gPackageInfo);
    self.arrPrepaid3gPackageInfo = [self.dbManager loadDataFromDB:queryPrepaid3g];
    //NSLog(@"Second array: %@",self.arrPrepaid3gPackageInfo);
    self.arrPostpaid2gPackageInfo = [self.dbManager loadDataFromDB:queryPostpaid2g];
    //NSLog(@"Third array: %@",self.arrPostpaid2gPackageInfo);
    self.arrPostpaid3gPackageInfo = [self.dbManager loadDataFromDB:queryPostpaid3g];
    //NSLog(@"Fourth array: %@",self.arrPostpaid3gPackageInfo);
    
    // Uncomment below NSLog lines to see if all the datas are fetched correctly.....
    /*
    NSLog(@"First array: %@",self.arrPrepaid2gPackageInfo);
    NSLog(@"Second array: %@",self.arrPrepaid3gPackageInfo);
    NSLog(@"Third array: %@",self.arrPostpaid2gPackageInfo);
    NSLog(@"Fourth array: %@",self.arrPostpaid3gPackageInfo);
    */
     // Reload the TableView
    [self.packageTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return @"Prepaid 2G Packages";
    } else if(section==1) {
        return @"Prepaid 3G Packages";
    } else if(section==2) {
        return @"Postpaid 2G Packages";
    } else {
        return @"Postpaid 3G Packages";
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0;
}

// Uncomment for a customized Header title of sections.....
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    MyTableViewHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier: @"headerCell"];
    
    //cell.titleLabel.text = [@"This is Title" uppercaseString];
    cell.titleLabel.text = [[tableView.dataSource tableView:tableView titleForHeaderInSection:section] uppercaseString];
    cell.firstLabel.text = @"Volume";
    cell.secondLabel.text = @"Price(BDT)";
    cell.thirdLabel.text = @"Validity(Days)";
    cell.fourthLabel.text = @"Activation(Dial)";
    [cell setSelected:NO animated:NO];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //// only the last variable's data is working............ /////
    
    // If this was only for single section
    //NSLog(@"numberOfRowsInSection %lu",(unsigned long)self.arrPostpaid2gPackageInfo.count);
    //return self.arrPostpaid3gPackageInfo.count;
    
    // For multiple sections
    if (section==0) {
        return self.arrPrepaid2gPackageInfo.count;
    } else if(section==1){
        return self.arrPrepaid3gPackageInfo.count;
    }else if (section==2){
        return self.arrPostpaid2gPackageInfo.count;
    }else{
        return self.arrPostpaid3gPackageInfo.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    // Configure the cell...

    NSInteger indexOfVolume = [self.dbManager.arrColumnNames indexOfObject:@"volume"];
    NSInteger indexOfPrice = [self.dbManager.arrColumnNames indexOfObject:@"price"];
    NSInteger indexOfValidity = [self.dbManager.arrColumnNames indexOfObject:@"validity"];
    NSInteger indexOfActivationCode = [self.dbManager.arrColumnNames indexOfObject:@"dial_recharge"];
    
    // Set the loaded data to the appropriate cell labels.
    if (indexPath.section==0) {
     
        cell.firstLabel.text = [NSString stringWithFormat:@"%@",
                                [[self.arrPrepaid2gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfVolume]];
        cell.secondLabel.text = [NSString stringWithFormat:@"%@",
                                 [[self.arrPrepaid2gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfPrice]];
        cell.thirdLabel.text = [NSString stringWithFormat:@"%@",
                                [[self.arrPrepaid2gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfValidity]];
        cell.fourthLabel.text = [NSString stringWithFormat:@"%@",
                                 [[self.arrPrepaid2gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfActivationCode]];
        
    }else if (indexPath.section==1) {
        cell.firstLabel.text = [NSString stringWithFormat:@"%@",
                                [[self.arrPrepaid3gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfVolume]];
        cell.secondLabel.text = [NSString stringWithFormat:@"%@",
                                 [[self.arrPrepaid3gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfPrice]];
        cell.thirdLabel.text = [NSString stringWithFormat:@"%@",
                                [[self.arrPrepaid3gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfValidity]];
        cell.fourthLabel.text = [NSString stringWithFormat:@"%@",
                                 [[self.arrPrepaid3gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfActivationCode]];
    }else if (indexPath.section==2) {
        cell.firstLabel.text = [NSString stringWithFormat:@"%@",
                                [[self.arrPostpaid2gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfVolume]];
        cell.secondLabel.text = [NSString stringWithFormat:@"%@",
                                 [[self.arrPostpaid2gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfPrice]];
        cell.thirdLabel.text = [NSString stringWithFormat:@"%@",
                                [[self.arrPostpaid2gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfValidity]];
        cell.fourthLabel.text = [NSString stringWithFormat:@"%@",
                                 [[self.arrPostpaid2gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfActivationCode]];
    }else {
        cell.firstLabel.text = [NSString stringWithFormat:@"%@",
                                [[self.arrPostpaid3gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfVolume]];
        cell.secondLabel.text = [NSString stringWithFormat:@"%@",
                                 [[self.arrPostpaid3gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfPrice]];
        cell.thirdLabel.text = [NSString stringWithFormat:@"%@",
                                [[self.arrPostpaid3gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfValidity]];
        cell.fourthLabel.text = [NSString stringWithFormat:@"%@",
                                 [[self.arrPostpaid3gPackageInfo objectAtIndex:indexPath.row] objectAtIndex:indexOfActivationCode]];
    }
    
    //cell.detailTextLabel.text = [NSString stringWithFormat:@"Age: %@",[[self.arrPackageInfo objectAtIndex:indexPath.row] objectAtIndex:3]];

    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section<3) {
        [self performSegueWithIdentifier:@"dialPackageInfo" sender:self];
    } else {
        [self performSegueWithIdentifier:@"rechargePackageInfo" sender:self];
    }
    
    //NSLog(@"Selected row in section: %lu",indexPath.section);
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController]. Done in the if-else section
    
    
    // Pass the selected object to the new view controller.
    NSIndexPath *touchedCell = [self.tableView indexPathForSelectedRow];
    NSArray *selectedPackageInCell = [[NSArray alloc]init];
    
    if (touchedCell.section==0) {
        DialPackageDetailsViewController *nextScene = [segue destinationViewController];
        selectedPackageInCell = self.arrPrepaid2gPackageInfo[touchedCell.row];
        nextScene.currentPackageInfo = selectedPackageInCell;
        nextScene.dbManager = self.dbManager;
    } else if (touchedCell.section==1) {
        DialPackageDetailsViewController *nextScene = [segue destinationViewController];
        selectedPackageInCell = self.arrPrepaid3gPackageInfo[touchedCell.row];
        nextScene.currentPackageInfo = selectedPackageInCell;
        nextScene.dbManager = self.dbManager;
    } else if (touchedCell.section==2) {
        DialPackageDetailsViewController *nextScene = [segue destinationViewController];
        selectedPackageInCell = self.arrPostpaid2gPackageInfo[touchedCell.row];
        nextScene.currentPackageInfo = selectedPackageInCell;
        nextScene.dbManager = self.dbManager;
    } else {
        RechargePackageDetailsViewController *nextScene2 = [segue destinationViewController];
        selectedPackageInCell = self.arrPostpaid3gPackageInfo[touchedCell.row];
        nextScene2.currentPackageInfo = selectedPackageInCell;
        nextScene2.dbManager = self.dbManager;
        //NSLog(@"Fourth section");
        
    }
    
    //NSArray *selectedPackageInCell = self.arrPrepaid3gPackageInfo[touchedCell.row];
    //nextScene.currentPackageInfo = selectedPackageInCell;
    //nextScene2.currentPackageInfo = selectedPackageInCell;
    //NSLog(@"Inside prepareForSegue method");
}


@end
