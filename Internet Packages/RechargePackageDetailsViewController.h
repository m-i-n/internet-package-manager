//
//  RechargePackageDetailsViewController.h
//  Internet Packages 2
//
//  Created by Nayem on 8/3/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface RechargePackageDetailsViewController : UIViewController

@property (nonatomic, strong) NSArray *currentPackageInfo;
@property (strong, nonatomic) DBManager *dbManager;
@end
