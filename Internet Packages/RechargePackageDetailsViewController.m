//
//  RechargePackageDetailsViewController.m
//  Internet Packages 2
//
//  Created by Nayem on 8/3/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import "RechargePackageDetailsViewController.h"

@interface RechargePackageDetailsViewController ()

@property (weak, nonatomic) IBOutlet UITextField *volumeTextField;
@property (weak, nonatomic) IBOutlet UITextField *priceTextField;
@property (weak, nonatomic) IBOutlet UITextField *validityTextField;
@property (weak, nonatomic) IBOutlet UITextField *usagetimeTextField;
@property (weak, nonatomic) IBOutlet UITextField *typeTextField;
@property (weak, nonatomic) IBOutlet UITextField *rechargeTextField;

@end

@implementation RechargePackageDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //NSLog(@"Recharge Package Details VC loaded");
    //NSLog(@"%@",self.currentPackageInfo);
    
    NSInteger indexOfVolume = [self.dbManager.arrColumnNames indexOfObject:@"volume"];
    NSInteger indexOfPrice = [self.dbManager.arrColumnNames indexOfObject:@"price"];
    NSInteger indexOfValidity = [self.dbManager.arrColumnNames indexOfObject:@"validity"];
    NSInteger indexOfType = [self.dbManager.arrColumnNames indexOfObject:@"generation"];
    NSInteger indexOfActivationCode = [self.dbManager.arrColumnNames indexOfObject:@"dial_recharge"];
    
    self.volumeTextField.text = [self.currentPackageInfo objectAtIndex:indexOfVolume];
    self.priceTextField.text = [self.currentPackageInfo objectAtIndex:indexOfPrice];
    self.validityTextField.text = [self.currentPackageInfo objectAtIndex:indexOfValidity];
    //self.usagetimeTextField.text = @"No DB Entry";
    self.typeTextField.text = [self.currentPackageInfo objectAtIndex:indexOfType];
    self.rechargeTextField.text = [self.currentPackageInfo objectAtIndex:indexOfActivationCode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
