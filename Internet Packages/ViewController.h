//
//  ViewController.h
//  Internet Packages 1
//
//  Created by Nayem on 7/22/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)buttonPressed:(id)sender;

@end

