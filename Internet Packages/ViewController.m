//
//  ViewController.m
//  Internet Packages 1
//
//  Created by Nayem on 7/22/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import "ViewController.h"
#import "Operators.h"
#import "PackageTableViewController.h"
@interface ViewController ()
@property (nonatomic, strong) Operators *currentOperator;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(id)sender {
    //UIButton *currentButton = (UIButton*)sender;
    self.currentOperator = [[Operators alloc]init];
    //currentOperator.name = currentButton.currentTitle;
    /// As sender is of type (id), to get properties of a button it should be cast to (UIButton) type
    self.currentOperator.name = [(UIButton*)sender currentTitle];
    //NSLog(@"Hello %@",self.currentOperator.name);
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    PackageTableViewController * nextScene = [segue destinationViewController];
    // Pass the selected object to the new view controller.
    nextScene.currentOperator = self.currentOperator;
}

@end
