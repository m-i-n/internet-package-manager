//
//  CustomTableViewCell.h
//  Temporary1
//
//  Created by Nayem on 7/18/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *firstLabel;
@property (nonatomic, strong) UILabel *secondLabel;
@property (nonatomic, strong) UILabel *thirdLabel;
@property (nonatomic, strong) UILabel *fourthLabel;

//@property (nonatomic, strong) UITableViewCell *accessoryIndicator;

@end
