//
//  MyTableViewCell.m
//  Temporary1
//
//  Created by Nayem on 7/18/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import "MyTableViewCell.h"

#define FIRST_LABEL_PERCENT 0.15
#define SECOND_LABEL_PERCENT 0.25
#define THIRD_LABEL_PERCENT 0.25
#define FOURTH_LABEL_PERCENT 0.30

//#define ACCESSORY_INDICATOR_PERCENT 0.10

@interface MyTableViewCell ()

@end

@implementation MyTableViewCell


///// CUSTOM CODE STARTS

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat startX = 16;
        CGFloat startY = 10;
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width - startX;
        
        // Initialize and Configure lebel //
        self.firstLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY, width *FIRST_LABEL_PERCENT, 25)];
        self.firstLabel.textColor = [UIColor colorWithRed:(188/255.f) green:(0/255.f) blue:(0/255.f) alpha:1.0];
        self.firstLabel.font = [UIFont boldSystemFontOfSize:13];
        [self.contentView addSubview:self.firstLabel];
        
        
        // Initialize and Configure lebel //
        self.secondLabel= [[UILabel alloc] initWithFrame:CGRectMake(startX+width *FIRST_LABEL_PERCENT, startY, width *SECOND_LABEL_PERCENT, 25)];
        self.secondLabel.font = [UIFont systemFontOfSize:11];
        self.secondLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.secondLabel];
        
        
        // Initialize and Configure lebel //
        self.thirdLabel= [[UILabel alloc] initWithFrame:CGRectMake(startX+width *(FIRST_LABEL_PERCENT + SECOND_LABEL_PERCENT), startY, width *THIRD_LABEL_PERCENT, 25)];
        self.thirdLabel.font = [UIFont systemFontOfSize:11];
        self.thirdLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.thirdLabel];
        
        
        // Initialize and Configure lebel //
        self.fourthLabel= [[UILabel alloc] initWithFrame:CGRectMake(startX+width *(FIRST_LABEL_PERCENT + SECOND_LABEL_PERCENT+THIRD_LABEL_PERCENT), startY, width *FOURTH_LABEL_PERCENT, 25)];
        self.fourthLabel.textColor = [UIColor colorWithRed:80.0/255.0 green:186.0/255.0 blue:248.0/255.0 alpha:1.0];
        self.fourthLabel.font = [UIFont boldSystemFontOfSize:13];
        [self.contentView addSubview:self.fourthLabel];
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        
    }
    return self;
}

///// CUSTOM CODE ENDS

@end
