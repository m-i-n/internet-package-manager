//
//  MyTableViewHeaderCell.h
//  Internet Packages 1
//
//  Created by Nayem on 7/25/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewHeaderCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *firstLabel;
@property (nonatomic, strong) UILabel *secondLabel;
@property (nonatomic, strong) UILabel *thirdLabel;
@property (nonatomic, strong) UILabel *fourthLabel;

@end
