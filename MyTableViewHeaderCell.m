//
//  MyTableViewHeaderCell.m
//  Internet Packages 1
//
//  Created by Nayem on 7/25/16.
//  Copyright © 2016 Nayem. All rights reserved.
//

#import "MyTableViewHeaderCell.h"

#define FIRST_LABEL_PERCENT 0.15
#define SECOND_LABEL_PERCENT 0.25
#define THIRD_LABEL_PERCENT 0.25
#define FOURTH_LABEL_PERCENT 0.35

@implementation MyTableViewHeaderCell

///// CUSTOM CODE STARTS

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGFloat startX = 16;
        CGFloat startY1 = 4;
        CGFloat startY2 = 24;
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        width = width - startX;
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY1, width, 18)];
        self.titleLabel.textColor = [UIColor darkGrayColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [self.contentView addSubview:self.titleLabel];
        
        // Initialize and Configure lebel //
        self.firstLabel = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY2, width *FIRST_LABEL_PERCENT, 14)];
        self.firstLabel.textColor = [UIColor grayColor];
        self.firstLabel.font = [UIFont boldSystemFontOfSize:10];
        [self.contentView addSubview:self.firstLabel];
        
        
        // Initialize and Configure lebel //
        self.secondLabel= [[UILabel alloc] initWithFrame:CGRectMake(startX+width *FIRST_LABEL_PERCENT, startY2, width *SECOND_LABEL_PERCENT, 14)];
        self.secondLabel.textColor = [UIColor grayColor];
        self.secondLabel.textAlignment = NSTextAlignmentCenter;
        self.secondLabel.font = [UIFont boldSystemFontOfSize:10];
        [self.contentView addSubview:self.secondLabel];
        
        
        // Initialize and Configure lebel //
        self.thirdLabel= [[UILabel alloc] initWithFrame:CGRectMake(startX+width *(FIRST_LABEL_PERCENT + SECOND_LABEL_PERCENT), startY2, width *THIRD_LABEL_PERCENT, 14)];
        self.thirdLabel.textColor = [UIColor grayColor];
        self.thirdLabel.textAlignment = NSTextAlignmentCenter;
        self.thirdLabel.font = [UIFont boldSystemFontOfSize:10];
        [self.contentView addSubview:self.thirdLabel];
        
        
        // Initialize and Configure lebel //
        self.fourthLabel= [[UILabel alloc] initWithFrame:CGRectMake(startX+width *(FIRST_LABEL_PERCENT + SECOND_LABEL_PERCENT+THIRD_LABEL_PERCENT), startY2, width *FOURTH_LABEL_PERCENT, 14)];
        self.fourthLabel.textColor = [UIColor grayColor];
        self.fourthLabel.font = [UIFont boldSystemFontOfSize:10];
        [self.contentView addSubview:self.fourthLabel];
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if(selected)
    {
        self.backgroundColor = [UIColor greenColor];
    }
    else
    {
        self.backgroundColor = [UIColor colorWithRed:218.0/255.0 green:218.0/255.0 blue:218.0/255.0 alpha:1.0];
    }
}

///// CUSTOM CODE ENDS

@end
